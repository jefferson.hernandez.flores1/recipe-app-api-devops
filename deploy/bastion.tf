#The bastion instance let us connect to our private network in AWS to administrate the resources

#A Data block let us retireve information to use it in the resources (the first "" are the name of the resource or what we consume from
# the cloud and the second "" are the name which this information will be keeped to used in the resources)
data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = [var.ami_linux]
  }
  owners = ["amazon"]
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = var.instance_type_micro

  # We can add a tags block to our resources, in this case, we define the name of the resource, the tags lets us add metadata to
  # our resources
  # tags = {
  #   Name = "${local.prefix}-bastion"
  # }

  # To use the common tags and just modify or add a tag (Name in this case) we use merge this way
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}