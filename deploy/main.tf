terraform {
  #Terraform BE to keep or save the tfstate in somewhere (in this case a S3 bucket)
  backend "s3" {
    bucket         = "recipe-app-api-devops-fstate-jeff"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

# This is how we create a dynamic variables inside Terraform
locals {
  # We can make interpolation in Terraform, this is the syntaxis
  # We set this local prefix to give a kind of 'tag' to our resources to understand where are they and what they do
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# This let us retrieve the region where we are 
data "aws_region" "current" {}
