variable "prefix" {
  # Always guve the variables a good, short, and descriptive content
  # We set a prefix to give a better description to the resources, so, this is how we can figure what are the resources for
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "email@managing.com"
}

variable "ami_linux" {
  #Replace the last numbers for a '*' in order to make the image run any version and the changes of versions doesn't matter
  default = "amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"
}

variable "instance_type_micro" {
  default = "t2.micro"
}