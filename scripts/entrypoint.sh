#!/bin/sh

set -e

#This lets us collect all the static files and store it in a single diretory
python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi

